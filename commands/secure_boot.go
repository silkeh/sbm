package commands

import "github.com/urfave/cli/v2"

// EnableSecureBoot enables secure boot.
func EnableSecureBoot(c *cli.Context) error {
	b, f, err := commonSetup(c)
	if err != nil {
		return err
	}
	defer f()

	if err := b.SecureBoot.Enroll(); err != nil {
		return err
	}

	if Flags.WithMicrosoft {
		if err := b.SecureBoot.EnrollMicrosoft(); err != nil {
			return err
		}
	}

	return nil
}
