package commands

import (
	"fmt"
	"os"
	"path/filepath"

	"github.com/urfave/cli/v2"
)

// ReportBooted reports the currently running kernel has booted successfully.
func ReportBooted(c *cli.Context) error {
	b, f, err := commonSetup(c)
	if err != nil {
		return err
	}
	defer f()

	dir := filepath.Join(b.Prefix, b.Config.KernelReportDir)
	if !exists(dir) {
		err = os.MkdirAll(dir, 0755)
		if err != nil {
			return fmt.Errorf("could not create directory: %s", err)
		}
	}

	file := filepath.Join(dir, fmt.Sprintf("k_booted_%s", b.Kernels.Current.KernelRelease()))
	if !exists(file) {
		err = os.WriteFile(file, []byte("solus-boot-manager file\n"), 0644)
		if err != nil {
			return fmt.Errorf("could not write report: %s", err)
		}
	}

	return nil
}

// exists returns true if the given prefix exists.
func exists(path string) bool {
	if _, err := os.Stat(path); os.IsNotExist(err) {
		return false
	}
	return true
}
