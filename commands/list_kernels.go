package commands

import (
	"fmt"
	"strings"

	"github.com/urfave/cli/v2"
)

// ListKernels prints a list of available kernels.
func ListKernels(c *cli.Context) error {
	b, f, err := commonSetup(c)
	if err != nil {
		return err
	}
	defer f()

	kernels, err := b.ListKernels()
	if err != nil {
		return err
	}

	for _, k := range kernels {
		var suffix []string
		if b.Kernels.Running(k) {
			suffix = append(suffix, "running")
		}
		if b.Kernels.DefaultBoot(k) {
			suffix = append(suffix, "boot")
		}

		name := k.Name()
		if suffix != nil {
			name += " (" + strings.Join(suffix, ", ") + ")"
		}
		fmt.Println(name)
	}

	return nil
}
