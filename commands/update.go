package commands

import (
	"fmt"
	"log"

	"github.com/urfave/cli/v2"
)

// Update performs a cleanup and update of the system.
func Update(c *cli.Context) error {
	bootManager, finalize, err := commonSetup(c)
	if err != nil {
		return err
	}
	defer finalize()

	// Run a cleanup in case we need disk space.
	log.Printf("Performing pre-update cleanup")
	if err := bootManager.Cleanup(); err != nil {
		return fmt.Errorf("error running cleanup: %w", err)
	}

	// Update the bootloaders and installed kernels.
	log.Printf("Updating boot loader and kernels")
	if err := bootManager.Update(); err != nil {
		return fmt.Errorf("error updating system: %w", err)
	}

	// Run the cleanup again to remove any outdated kernels.
	log.Printf("Performing post-update cleanup")
	if err := bootManager.Cleanup(); err != nil {
		return fmt.Errorf("error running cleanup: %w", err)
	}

	return nil
}
