package commands

import (
	"fmt"
	"os"
	"path"
	"strconv"

	"github.com/urfave/cli/v2"
)

// GetTimeout gets the timeout of the bootloader.
func GetTimeout(c *cli.Context) error {
	b, f, err := commonSetup(c)
	if err != nil {
		return err
	}
	defer f()

	timeout, err := b.Bootloader.GetTimeout()
	if err == nil {
		fmt.Printf("Timeout: %v seconds\n", timeout)
	}

	return err
}

// SetTimeout sets the timeout of the bootloader.
func SetTimeout(c *cli.Context) error {
	if c.NArg() != 1 {
		return fmt.Errorf("set-timeout requires exactly 1 argument")
	}

	b, f, err := commonSetup(c)
	if err != nil {
		return err
	}
	defer f()

	timeout, err := strconv.Atoi(c.Args().First())
	if err != nil {
		return err
	}

	// Set /etc/kernel/timeout for backwards compatibility
	err = os.WriteFile(
		path.Join(b.Config.KernelConfigDir, "timeout"),
		[]byte(fmt.Sprintf("%v\n", timeout)), 0644)
	if err != nil {
		return err
	}

	mounted, err := b.MountBoot()
	if err != nil {
		return err
	}
	if mounted {
		defer b.UnmountBoot()
	}

	return b.Bootloader.SetTimeout(timeout)
}
