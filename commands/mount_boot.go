package commands

import (
	"github.com/urfave/cli/v2"
)

// MountBoot mounts the boot partition.
func MountBoot(c *cli.Context) error {
	_, _, err := commonSetup(c)
	if err != nil {
		return err
	}
	return err
}
