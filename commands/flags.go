package commands

// Flags contain the command line flags.
var Flags struct {
	Prefix        string
	ImageMode     bool
	NoEFI         bool
	Verbose       bool
	WithMicrosoft bool
}
