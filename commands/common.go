package commands

import (
	"io/ioutil"
	"log"

	"github.com/urfave/cli/v2"

	"gitlab.com/silkeh/sbm/bootmanager"
	"gitlab.com/silkeh/sbm/config"
)

// commonSetup contains the setup for common commands.
func commonSetup(c *cli.Context) (*bootmanager.BootManager, func(), error) {
	if !Flags.Verbose {
		log.SetOutput(ioutil.Discard)
	}

	b, err := bootmanager.NewBootManager(config.Config, Flags.Prefix)
	if err != nil {
		return nil, nil, err
	}

	f := func() {}
	if b.MountedBoot {
		f = func() {
			err := b.UnmountBoot()
			if err != nil {
				log.Println(err)
			}
		}
	}

	return b, f, nil
}
