package commands

import (
	"fmt"
	"log"

	"github.com/urfave/cli/v2"

	"gitlab.com/silkeh/sbm/kernel"
)

// SetKernel sets the default kernel
func SetKernel(c *cli.Context) error {
	if c.NArg() != 1 {
		return fmt.Errorf("set-kernel requires exactly 1 argument")
	}

	b, f, err := commonSetup(c)
	if err != nil {
		return err
	}
	defer f()

	kernels, err := b.ListKernels()
	if err != nil {
		return err
	}

	var kernel *kernel.Kernel
	for _, k := range kernels {
		if k.Name() == c.Args().First() {
			kernel = k
			break
		}
	}

	if kernel == nil {
		return fmt.Errorf("unknown kernel: %q", c.Args().First())
	}

	log.Printf("Setting default kernel to %s", kernel.Name())
	return b.Bootloader.SetDefaultKernel(kernel)
}

// InstallKernel sets the default kernel
func InstallKernel(c *cli.Context) error {
	if c.NArg() != 1 {
		return fmt.Errorf("install-kernel requires exactly 1 argument")
	}

	b, f, err := commonSetup(c)
	if err != nil {
		return err
	}
	defer f()

	kernels, err := b.ListKernels()
	if err != nil {
		return err
	}

	var kernel *kernel.Kernel
	for _, k := range kernels {
		if k.Name() == c.Args().First() {
			kernel = k
			break
		}
	}

	if kernel == nil {
		return fmt.Errorf("unknown kernel: %q", c.Args().First())
	}

	log.Printf("Installing %s to the bootloader", kernel.Name())
	err = b.InstallKernel(kernel)
	if err != nil {
		return err
	}

	log.Printf("Setting default kernel to %s", kernel.Name())
	return b.Bootloader.SetDefaultKernel(kernel)
}
