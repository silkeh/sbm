package linux

import (
	"bytes"

	"golang.org/x/sys/unix"
)

// UTSName is equivalent to unix.Utsname,
// but with strings instead of byte arrays.
type UTSName struct {
	SysName    string // name of the operating system
	NodeName   string // name of the node within a network
	Release    string // release of the kernel
	Version    string //
	Machine    string
	DomainName string
}

// strConv converts a null-terminated string contained
// in a byte slice into a string.
func strConv(b []byte) string {
	n := bytes.IndexByte(b[:], 0)
	return string(b[:n])
}

// UName returns the UTSName for this system.
func UName() (u UTSName, err error) {
	buf := new(unix.Utsname)
	err = unix.Uname(buf)
	if err != nil {
		return
	}

	return UTSName{
		SysName:    strConv(buf.Sysname[:]),
		NodeName:   strConv(buf.Nodename[:]),
		Release:    strConv(buf.Release[:]),
		Version:    strConv(buf.Version[:]),
		Machine:    strConv(buf.Machine[:]),
		DomainName: strConv(buf.Domainname[:]),
	}, nil
}
