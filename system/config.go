package system

// Config represents the system configuration
type Config struct {
	BootDir          string // Default boot directory.
	KernelConfigDir  string // Directory containing additional kernel configuration.
	KernelDir        string // Directory containing kernels.
	KernelModulesDir string // Directory containing kernel modules.
	KernelNamespace  string // Kernel namespace used by this system.
	KernelReportDir  string // Kernel report directory.
	VendorPrefix     string // System vendor prefix.
	SecureBootDir    string // Directory containing secure boot state.
	MSSecureBootDir  string // Directory containing Microsoft secure boot data.
	ForceLegacy      bool   // Flag to force legacy boot instead of EFI.
}
