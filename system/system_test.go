package system

import (
	"testing"
)

var (
	// currentSystem contains the actual current system
	currentSystem = New("/", false)

	// efiSystem is an EFI system based on test fixtures.
	efiSystem = &System{Linux: &testLinux{linux: linux{prefix: "../fixtures/efi"}}, forceLegacy: false}

	// espSystem is an EFI system with the boot partition detected via ESP instead of EFI variables
	espSystem = &System{Linux: &testLinux{linux: linux{prefix: "../fixtures/esp"}}, forceLegacy: false}

	// efiLegacySystem is an EFI system forced to legacy mode based on test fixtures.
	efiLegacySystem = &System{Linux: &testLinux{linux: linux{prefix: "../fixtures/efi"}}, forceLegacy: true}

	// legacySystem is a legacy system based on test fixtures.
	legacySystem = &System{Linux: &testLinux{linux: linux{prefix: "../fixtures/legacy"}}, forceLegacy: false}
)

func TestSystem_BootFSType(t *testing.T) {
	tests := map[*System]string{
		efiSystem: "vfat",
		espSystem: "vfat",
	}

	for s, exp := range tests {
		fsType, err := s.BootFSType()
		if err != nil {
			t.Errorf("BootFSType error: %s", err)
			continue
		}

		if fsType != exp {
			t.Errorf("BootFSType incorrect: expected %q, got %q", exp, fsType)
		}
	}
}

func TestSystem_IsEFI(t *testing.T) {
	tests := map[*System]bool{
		efiSystem:       true,
		espSystem:       true,
		efiLegacySystem: false,
		legacySystem:    false,
	}

	for s, exp := range tests {
		if s.IsEFI() != exp {
			t.Errorf("IsEFI incorrect: expected %v, got %v", exp, s.IsEFI())
		}
	}
}

func TestSystem_BootDevice(t *testing.T) {
	tests := map[*System]string{
		efiSystem: "../fixtures/efi/dev/disk/by-partuuid/e90f44b5-bb8a-41af-b680-b0bf5b0f2a65",
		espSystem: "../fixtures/esp/dev/disk/by-partlabel/ESP",
	}

	for s, exp := range tests {
		device, err := s.BootDevice()
		if err != nil {
			t.Errorf("BootDevice error: %s", err)
			continue
		}

		if device != exp {
			t.Errorf("BootDevice incorrect: expected %q, got %q", exp, device)
		}
	}
}

func TestSystem_IsMounted(t *testing.T) {
	tests := map[string]bool{
		"/":                 true,
		"/aevuquoo9ugeij1W": false,
	}

	for m, exp := range tests {
		if got := currentSystem.IsMounted(m); got != exp {
			t.Errorf("incorrect for %q: expected %v, got %v", m, exp, got)
		}
	}
}

func TestSystem_GetDeviceForMountPoint(t *testing.T) {
	tests := map[*System]string{
		efiSystem: "/dev/sda2",
	}

	for s, exp := range tests {
		device, err := s.GetDeviceForMountPoint("/")
		if err != nil {
			t.Errorf("error: %s", err)
			continue
		}

		if device != exp {
			t.Errorf("incorrect: expected %q, got %q", exp, device)
		}
	}
}

func TestSystem_GetMountPointForDevice(t *testing.T) {
	tests := map[*System]string{
		efiSystem: "/dev/sda2",
	}

	for s, dev := range tests {
		exp := "/"
		mnt, err := s.GetMountPointForDevice(dev)
		if err != nil {
			t.Errorf("error: %s", err)
			continue
		}

		if mnt != exp {
			t.Errorf("incorrect: expected %q, got %q", exp, mnt)
		}
	}
}

func TestSystem_DeviceInfo(t *testing.T) {
	tests := map[*System]struct {
		in  string
		out *DeviceInfo
	}{
		efiSystem: {
			in: "../fixtures/efi/dev/disk/by-partuuid/9be1c23e-7723-4e49-bf66-fa5c2ddc16bd",
			out: &DeviceInfo{
				UUID:           "9be1c23e-7723-4e49-bf66-fa5c2ddc16bd",
				LuksUUID:       "d6ae1c6e-7ad7-43d4-9756-deed3aa51d9a",
				BtrfsSubVolume: "/solus",
			},
		},
	}

	for s, test := range tests {
		info, err := s.DeviceInfo(test.in)
		if err != nil {
			t.Errorf("DeviceInfo error: %s", err)
			continue
		}

		if info.UUID != test.out.UUID {
			t.Errorf("UUID incorrect: expected %q, got %q", test.out.UUID, info.UUID)
		}
		if info.LuksUUID != test.out.LuksUUID {
			t.Errorf("UUID incorrect: expected %q, got %q", test.out.LuksUUID, info.LuksUUID)
		}
		if info.BtrfsSubVolume != test.out.BtrfsSubVolume {
			t.Errorf("UUID incorrect: expected %q, got %q", test.out.BtrfsSubVolume, info.BtrfsSubVolume)
		}
	}
}
