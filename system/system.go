package system

import (
	"fmt"
	"os"
	"path"
	"path/filepath"
	"strings"
)

// System represents the running/target operating system.
type System struct {
	Linux

	prefix      string
	forceLegacy bool
}

// New returns a new System.
func New(prefix string, forceLegacy bool) *System {
	return &System{
		Linux:       new(linux),
		prefix:      prefix,
		forceLegacy: forceLegacy,
	}
}

// BootFSType returns the filesystem type of the boot device.
func (s *System) BootFSType() (string, error) {
	dev, err := s.BootDevice()
	if err != nil {
		return "", err
	}

	i, err := s.GetBlockDevice(dev)
	if err != nil {
		return "", err
	}
	return i.FSType, nil
}

// IsEFI returns true if the system supports EFI boots.
func (s *System) IsEFI() bool {
	if s.forceLegacy {
		return false
	}

	if _, err := os.Stat(path.Join(s.SysFSPath("firmware/efi"))); err == nil {
		return true
	}

	return false
}

// BootDevice returns the path to the boot device.
func (s *System) BootDevice() (string, error) {
	if s.forceLegacy || !s.IsEFI() {
		return s.LegacyBootDevice()
	}
	return s.EFIBootDevice()
}

// EFIBootDevice returns the path to the EFI partition.
func (s *System) EFIBootDevice() (string, error) {
	// Try to resolve via the EFI vars.
	loaderDevice := s.glob(s.SysFSPath("firmware/efi/efivars/LoaderDevicePartUUID-*"))
	if loaderDevice != "" {
		uuid, err := os.ReadFile(loaderDevice)
		if err != nil {
			return "", fmt.Errorf("unable to read LoaderDevicePartUUID: %w", err)
		}

		disk := s.DevFSPath("disk/by-partuuid", sanitizeUUID(uuid))
		if _, err := os.Stat(disk); err == nil {
			return filepath.EvalSymlinks(disk)
		}
	}

	// Resolve via ESP label
	disk := s.DevFSPath("disk/by-partlabel/ESP")
	if _, err := os.Stat(disk); err == nil {
		return filepath.EvalSymlinks(disk)
	}

	return "", fmt.Errorf("unable to find boot device")
}

// LegacyBootDevice returns the path or symlink to the legacy boot partition.
func (s *System) LegacyBootDevice() (string, error) {
	panic("implement me!") // TODO
}

// IsMounted returns true if the given target is mounted.
func (s *System) IsMounted(target string) bool {
	dev, err := s.GetDeviceForMountPoint(target)
	return err == nil && dev != ""
}

// GetDeviceForMountPoint returns the block device for a mount point.
func (s *System) GetDeviceForMountPoint(target string) (string, error) {
	return getMounts(s.ProcFSPath("self/mounts"), target, 1, 0)
}

// GetMountPointForDevice returns the mount point for a block device.
func (s *System) GetMountPointForDevice(dev string) (string, error) {
	return getMounts(s.ProcFSPath("self/mounts"), dev, 0, 1)
}

// DeviceInfo returns the DeviceInfo for a device.
func (s *System) DeviceInfo(dev string) (*DeviceInfo, error) {
	info := new(DeviceInfo)

	// Resolve symlinks
	dev, err := filepath.EvalSymlinks(dev)
	if err != nil {
		return nil, err
	}

	// Get block device info
	fsInfo, err := s.GetFilesystem(dev)
	if err != nil {
		return nil, err
	}

	// Set UUID
	info.UUID = fsInfo.UUID

	// Get subvolume for Btrfs
	if fsInfo.FStype == "btrfs" && fsInfo.FSroot != "" {
		info.BtrfsSubVolume = fsInfo.FSroot
	}

	// Get LUKS UUID
	if strings.HasPrefix(path.Base(dev), "dm-") {
		uuid, err := s.resolveLUKS(path.Base(dev))
		if err != nil {
			return nil, err
		}

		if uuid != "" {
			info.LuksUUID = uuid
		}
	}

	return info, nil
}

func (s *System) resolveLUKS(dm string) (string, error) {
	// Resolve nested device
	d := s.glob(s.SysFSPath("block", dm, "/slaves/*/slaves/*/dev"))
	if d == "" {
		d = s.glob(s.SysFSPath("block", dm, "/slaves/*/dev"))
		if d == "" {
			return "", nil
		}
	}

	// Read reference
	majMin, err := os.ReadFile(d)
	if err != nil {
		return "", err
	}

	// Get info for underlying device
	luksDev, err := s.GetBlockDevice(s.DevFSPath("block", strings.TrimRight(string(majMin), "\n")))
	if err != nil {
		return "", err
	}

	if luksDev.FSType == "crypto_LUKS" {
		return luksDev.UUID, nil
	}

	return "", nil
}

// glob matches a glob and returns the result, or empty on no match.
func (s *System) glob(glob string) string {
	matches, err := filepath.Glob(glob)
	if err != nil || len(matches) == 0 {
		return ""
	}

	return matches[0]
}
