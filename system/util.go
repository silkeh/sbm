package system

import (
	"bufio"
	"errors"
	"fmt"
	"io/fs"
	"io/ioutil"
	"os"
	"strings"
	"unicode"
)

var uuidRanges = []*unicode.RangeTable{unicode.ASCII_Hex_Digit, unicode.Hyphen, unicode.Dash}

func sanitizeUUID(uuid []byte) (u string) {
	for _, b := range uuid {
		r := rune(b)
		if !unicode.IsOneOf(uuidRanges, r) {
			continue
		}
		if r == '_' {
			r = '-'
		}
		if unicode.IsUpper(r) {
			r = unicode.ToLower(r)
		}
		u += string(r)
	}
	return
}

// getMounts finds an entry in an mtab file by index.
// The indexes are: 0) device, 1) mount point, 2) fstype, 3) options, 4) flags
func getMounts(path, match string, matchIdx, getIdx int) (string, error) {
	file, err := os.Open(path)
	if err != nil {
		return "", err
	}
	defer file.Close()

	scanner := bufio.NewScanner(file)
	for scanner.Scan() {
		parts := strings.SplitN(scanner.Text(), " ", 5)
		if len(parts) != 5 {
			continue
		}
		if parts[matchIdx] == match {
			return parts[getIdx], nil
		}
	}

	return "", fmt.Errorf("no mount entry for %q", match)
}

// IsEmptyDir returns true if the given directory is empty or does not exist.
func IsEmptyDir(path string) bool {
	files, err := ioutil.ReadDir(path)
	return (err == nil || errors.Is(err, fs.ErrNotExist)) && len(files) == 0
}
