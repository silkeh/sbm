package system

import (
	"fmt"
	"path"
	"syscall"

	"gitlab.com/silkeh/sbm/util"
)

// Linux abstracts Linux paths and syscalls.
type Linux interface {
	// DevFSPath joins the given elements to the default DevFS path (typically /dev).
	DevFSPath(...string) string

	// ProcFSPath joins the given elements to the default DevFS path (typically /proc).
	ProcFSPath(...string) string

	// SysFSPath joins the given elements to the default DevFS path (typically /sys).
	SysFSPath(...string) string

	// Mount mounts a filesystem of a certain type in a target location.
	Mount(dev, target, fsType string) error

	// Unmount unmounts a mounted filesystem.
	Unmount(target string) error

	// GetBlockDevice returns the block device info for a device.
	GetBlockDevice(dev string) (*util.BlockDevice, error)

	// GetFilesystem returns the filesystem info for a device.
	GetFilesystem(dev string) (*util.Filesystem, error)
}

// linux is the basic Linux implementation.
type linux struct {
	prefix string
}

// DevFSPath joins the given elements to the default DevFS path (typically /dev).
func (l *linux) DevFSPath(elem ...string) string {
	return l.joinPath("/dev", elem)
}

// ProcFSPath joins the given elements to the default DevFS path (typically /proc).
func (l *linux) ProcFSPath(elem ...string) string {
	return l.joinPath("/proc", elem)
}

// SysFSPath joins the given elements to the default DevFS path (typically /sys).
func (l *linux) SysFSPath(elem ...string) string {
	return l.joinPath("/sys", elem)
}

// joinPath joins the given path and elements with the configured prefix.
func (l *linux) joinPath(p string, elems []string) string {
	return path.Join(append([]string{l.prefix, p}, elems...)...)
}

// Mount mounts a filesystem of a certain type in a target location.
func (l *linux) Mount(dev, target, fsType string) error {
	return syscall.Mount(dev, target, fsType, syscall.MS_MGC_VAL, "")
}

// Unmount unmounts a mounted filesystem.
func (l *linux) Unmount(target string) error {
	return syscall.Unmount(target, 0)
}

// GetBlockDevice returns the block device info for a device.
func (l *linux) GetBlockDevice(dev string) (*util.BlockDevice, error) {
	devs, err := util.BlockDevices(dev)
	if err != nil {
		return nil, err
	}

	if len(devs) > 0 {
		return devs[0], nil
	}

	return nil, fmt.Errorf("unable to find block device %q", dev)
}

// GetFilesystem returns the filesystem info for a device.
func (l *linux) GetFilesystem(path string) (*util.Filesystem, error) {
	devs, err := util.Filesystems(path)
	if err != nil {
		return nil, err
	}

	if len(devs) > 0 {
		return devs[0], nil
	}

	return nil, fmt.Errorf("unable to find block device %q", path)
}
