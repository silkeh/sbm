package system

import (
	"fmt"
	"path"

	"gitlab.com/silkeh/sbm/util"
)

type testLinux struct {
	linux
}

func (l *testLinux) GetBlockDevice(dev string) (*util.BlockDevice, error) {
	switch path.Base(dev) {
	case "e90f44b5-bb8a-41af-b680-b0bf5b0f2a65", "ESP":
		return &util.BlockDevice{FSType: "vfat"}, nil
	case "255:2":
		return &util.BlockDevice{FSType: "crypto_LUKS", UUID: "d6ae1c6e-7ad7-43d4-9756-deed3aa51d9a"}, nil
	default:
		return nil, fmt.Errorf("unable to find block device %q", dev)
	}
}

func (l *testLinux) GetFilesystem(dev string) (*util.Filesystem, error) {
	switch path.Base(dev) {
	case "dm-1":
		return &util.Filesystem{UUID: "9be1c23e-7723-4e49-bf66-fa5c2ddc16bd", FStype: "btrfs", FSroot: "/solus"}, nil
	default:
		return nil, fmt.Errorf("unable to find block device %q", dev)
	}
}
