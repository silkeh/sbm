package system

// DeviceInfo represents a root device.
type DeviceInfo struct {
	UUID           string
	LuksUUID       string
	BtrfsSubVolume string
}
