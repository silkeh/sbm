module gitlab.com/silkeh/sbm

go 1.16

require (
	github.com/cpuguy83/go-md2man/v2 v2.0.1 // indirect
	github.com/foxboron/go-uefi v0.0.0-20211009202040-21cbdd77b3b1
	github.com/foxboron/sbctl v0.0.0-20211024102802-fca627a83c83
	github.com/google/uuid v1.3.0
	github.com/urfave/cli/v2 v2.3.0
	golang.org/x/sys v0.0.0-20211031064116-611d5d643895
	golang.org/x/text v0.3.7 // indirect
)
