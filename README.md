# SBM

SBM is a **very much WIP** boot manager with a design based on [clr-boot-manager][].
The goal of SBM is to provide a boot manager that is backwards compatible with clr-boot-manager,
but with the following additional features:

- EFI Unified Kernel images from the [Boot Loader Specification][efi-unified-kernel-images].
- Secure boot support based on [sbctl][].
- Integration with full disk encryption using [systemd-cryptenroll][].

[clr-boot-manager]: https://github.com/clearlinux/clr-boot-manager
[sbctl]: https://github.com/foxboron/sbctl
[efi-unified-kernel-images]: https://systemd.io/BOOT_LOADER_SPECIFICATION/#type-2-efi-unified-kernel-images
[systemd-cryptenroll]: https://www.freedesktop.org/software/systemd/man/systemd-cryptenroll.html
