package bootmanager

import (
	"gitlab.com/silkeh/sbm/kernel"
)

// Kernels contains the state of the current and default kernels
type Kernels struct {
	// Current contains the currently running kernel.
	Current *kernel.Kernel

	// Default contains the kernel set as default boot.
	Default *kernel.Kernel
}

// NextDefault returns the latest kernel version that has the same type as the current kernel.
// The list of kernels given to this function must be sorted.
func (k *Kernels) NextDefault(kernels []*kernel.Kernel) (next *kernel.Kernel) {
	// Find the first matching type
	for _, kernel := range kernels {
		if kernel.Type == k.Current.Type {
			return kernel
		}
	}

	// Return the latest as fallback
	if len(kernels) > 0 {
		return kernels[0]
	}

	return nil
}

// Running returns true if the kernel is currently running.
func (k *Kernels) Running(kernel *kernel.Kernel) bool {
	return kernel.Name() == k.Current.Name()
}

// DefaultBoot returns true if the kernel is the current default boot.
func (k *Kernels) DefaultBoot(kernels *kernel.Kernel) bool {
	return kernels.Name() == k.Default.Name()
}
