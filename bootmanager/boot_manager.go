package bootmanager

import (
	"errors"
	"fmt"
	"io/fs"
	"io/ioutil"
	"log"
	"os"
	"path"
	"sort"

	"gitlab.com/silkeh/sbm/bootloader"
	"gitlab.com/silkeh/sbm/kernel"
	"gitlab.com/silkeh/sbm/linux"
	"gitlab.com/silkeh/sbm/system"
)

// BootManager manages the installed kernels via bootloaders.
type BootManager struct {
	// Config contains the system configuration.
	Config *system.Config

	// Prefix contains the filesystem prefix on which to operate.
	Prefix string

	// Kernels contains the current and default kernels.
	Kernels *Kernels

	// System contains the OS/System abstraction.
	System *system.System

	// Bootloader contains the current bootloader.
	Bootloader bootloader.Bootloader

	// SecureBoot manages secure boot.
	SecureBoot *bootloader.SecureBoot

	// MountedBoot indicates that boot was mounted by this process.
	MountedBoot bool
}

// NewBootManager initializes a boot manager for a prefix.
func NewBootManager(config *system.Config, prefix string) (*BootManager, error) {
	uts, err := linux.UName()
	if err != nil {
		return nil, fmt.Errorf("cannot determine uname: %w", err)
	}

	b := &BootManager{
		Config: config,
		Prefix: prefix,
		System: system.New(prefix, config.ForceLegacy),
	}

	b.SecureBoot = &bootloader.SecureBoot{Config: config}
	b.Bootloader = bootloader.NewSystemdBoot(config, prefix, b.SecureBoot)

	_, err = b.MountBoot()
	if err != nil {
		return nil, err
	}

	currentKernel, err := kernel.New(config.KernelNamespace, uts.Release, prefix)
	if err != nil {
		return nil, err
	}

	defaultKernel, err := b.Bootloader.GetDefaultKernel()
	if err != nil {
		return nil, err
	}

	b.Kernels = &Kernels{Current: currentKernel, Default: defaultKernel}

	return b, nil
}

// Update updates the bootloaders and installed kernels.
func (b *BootManager) Update() error {
	// Initialize secure boot if required.
	if !b.SecureBoot.Initialized() {
		if err := b.SecureBoot.GenerateKeys(); err != nil {
			return err
		}
	}

	// Install the bootloader if required.
	if b.Bootloader.NeedsInstall() {
		log.Printf("Installing bootloader: %s", b.Bootloader.Name())
		if err := b.Bootloader.Install(); err != nil {
			return err
		}
	}

	// Update the bootloader if required.
	if b.Bootloader.NeedsUpdate() {
		log.Printf("Updating bootloader: %s", b.Bootloader.Name())
		if err := b.Bootloader.Update(); err != nil {
			return err
		}
	}

	// Get a list of available kernels per type.
	kernels, err := b.ListKernels()
	if err != nil {
		return err
	}

	// (Re)install latest non-installed kernels
	err = b.installKernels(kernels)
	if err != nil {
		return err
	}

	// Set default boot
	if err = b.setDefaultKernel(b.Kernels.NextDefault(kernels)); err != nil {
		return err
	}

	return nil
}

// installKernels (re)installs latest non-installed kernels
func (b *BootManager) installKernels(kernels []*kernel.Kernel) error {
	for _, k := range kernel.List(kernels).Latest() {
		// Remove next kernel if not running or default boot.
		// This allows for a CBM -> SBM transition.
		if !b.Kernels.DefaultBoot(k) && !b.Kernels.Running(k) {
			log.Printf("Removing kernel: %s", k)
			if err := b.Bootloader.RemoveKernel(k); err != nil {
				return fmt.Errorf("unable to remove kernel %s: %w", k, err)
			}
		}

		log.Printf("Installing kernel: %s", k)
		if err := b.InstallKernel(k); err != nil {
			return fmt.Errorf("unable to install kernel %s: %w", k, err)
		}
	}

	return nil
}

// setDefaultKernel sets the default kernel.
func (b *BootManager) setDefaultKernel(kernel *kernel.Kernel) error {
	if kernel == nil {
		return fmt.Errorf("invalid default kernel")
	}

	log.Printf("Set default boot to %s", kernel)
	if err := b.Bootloader.SetDefaultKernel(kernel); err != nil {
		return err
	}

	// Update default boot
	b.Kernels.Default = kernel

	return nil
}

func (b *BootManager) lastBootedKernels(kernels []*kernel.Kernel) (latest []*kernel.Kernel) {
	lastType := ""
	for _, k := range kernels {
		if k.Type != lastType && b.KernelBooted(k) {
			lastType = k.Type
			latest = append(latest, k)
		}
	}
	return
}

// Cleanup removes outdated or unneeded kernels.
// All kernels are removed unless they meet any of the following criteria:
// - It is currently running
// - It is set as default boot
// - It is the latest kernel currently installed.
// - It is the last known successfully booted kernel.
func (b *BootManager) Cleanup() error {
	preserve := kernel.List{b.Kernels.Current, b.Kernels.Default}
	kernels, err := b.ListKernels()
	if err != nil {
		return err
	}

	// Preserve latest kernels
	preserve = append(preserve, kernel.List(kernels).Latest()...)

	// Preserve latest booted kernels
	preserve = append(preserve, b.lastBootedKernels(kernels)...)

	log.Printf("Preserving kernels: %v", preserve)

	// Remove system kernels
	for _, k := range kernels {
		if preserve.Contains(k) {
			continue
		}

		log.Printf("Removing system kernel: %s", k)
		if err = b.RemoveSystemKernel(k); err != nil {
			return err
		}
	}

	// Get bootable kernels
	kernels, err = b.Bootloader.ListKernels()
	if err != nil {
		return err
	}

	// Perform cleanup of bootable kernels
	for _, k := range kernels {
		if preserve.Contains(k) {
			continue
		}

		log.Printf("Removing bootable kernel: %s", k)
		if err := b.Bootloader.RemoveKernel(k); err != nil {
			return fmt.Errorf("unable to remove kernel %s: %w", k, err)
		}
	}

	return nil
}

// InstallKernel installs a kernel.
func (b *BootManager) InstallKernel(k *kernel.Kernel) error {
	// Update kernel cmdline
	if err := b.updateCmdLine(k); err != nil {
		return err
	}

	return b.Bootloader.InstallKernel(k)
}

// RemoveSystemKernel removes a kernel from the system.
func (b *BootManager) RemoveSystemKernel(k *kernel.Kernel) error {
	// Remove kernel files
	if err := k.Remove(); err != nil {
		return fmt.Errorf("unable to remove kernel: %w", err)
	}

	// Remove header files
	if err := os.RemoveAll(path.Join(b.Prefix, "usr", "src", "linux-headers-"+k.KernelRelease())); err != nil {
		return fmt.Errorf("unable to remove kernel headers: %w", err)
	}

	// Remove kernel modules
	if err := os.RemoveAll(path.Join(b.Prefix, b.Config.KernelModulesDir, k.KernelRelease())); err != nil {
		return fmt.Errorf("unable to remove kernel modules: %w", err)
	}

	return nil
}

func (b *BootManager) updateCmdLine(k *kernel.Kernel) (err error) {
	k.SysCmdLine, err = b.sysCmdLine()
	if err != nil {
		return
	}

	k.UserCmdLine, err = b.userCmdLine()
	return
}

func (b *BootManager) sysCmdLine() (string, error) {
	dev, err := b.System.GetDeviceForMountPoint(b.Prefix)
	if err != nil {
		return "", err
	}

	deviceInfo, err := b.System.DeviceInfo(dev)
	if err != nil {
		return "", err
	}

	cmdLine := "root=UUID=" + deviceInfo.UUID
	if deviceInfo.LuksUUID != "" {
		cmdLine += " rd.luks.uuid=" + deviceInfo.LuksUUID
	}
	if deviceInfo.BtrfsSubVolume != "" {
		cmdLine += " rootflags=subvol=" + deviceInfo.BtrfsSubVolume
	}

	return cmdLine, err
}

func (b *BootManager) userCmdLine() (string, error) {
	cmdline, err := ParseCmdLineDir(b.Config.KernelDir, b.Config.KernelConfigDir)
	if err != nil {
		return "", err
	}

	return ParseCmdLineRemovalDir(cmdline, b.Config.KernelDir)
}

// ListKernels returns a sorted list of available kernels.
func (b *BootManager) ListKernels() ([]*kernel.Kernel, error) {
	dir := path.Join(b.Prefix, b.Config.KernelDir)
	files, err := ioutil.ReadDir(dir)
	if err != nil {
		return nil, err
	}

	kernels := make([]*kernel.Kernel, 0, len(files))
	for _, f := range files {
		// Skip non-regular and empty files
		if !f.Mode().IsRegular() || f.Size() == 0 {
			continue
		}

		// Load the file as a kernel
		k, err := kernel.FromPath(dir, f.Name())
		if err != nil {
			log.Printf("Skip potential kernel file %q: %s", f.Name(), err)
			continue
		}

		// Skip invalid
		if k.Namespace != b.Config.KernelNamespace {
			log.Printf("Not our kernel: %s\n", f.Name())
			continue
		}

		kernels = append(kernels, k)
	}

	// Sort the kernels by type and release
	sort.Sort(kernel.List(kernels))

	return kernels, nil
}

// KernelBooted returns true if the kernel has been booted.
func (b *BootManager) KernelBooted(k *kernel.Kernel) bool {
	_, err := os.Stat(path.Join(b.ReportDir(), "k_booted_"+k.KernelRelease()))
	return err == nil
}

// BootDirectory returns the boot directory.
func (b *BootManager) BootDirectory() string {
	return path.Join(b.Prefix, b.Config.BootDir)
}

// ReportDir returns the report directory.
func (b *BootManager) ReportDir() string {
	return path.Join(b.Prefix, b.Config.KernelReportDir)
}

// BootDevice returns the boot device.
func (b *BootManager) BootDevice() (string, error) {
	if !b.Config.ForceLegacy && b.System.IsEFI() {
		return b.System.EFIBootDevice()
	}

	return b.System.LegacyBootDevice()
}

// IsBootPartitionLess returns true if the system has been configured for
// booting without a boot partition.
func (b *BootManager) IsBootPartitionLess() bool {
	return b.Bootloader.Capabilities().PartitionLess &&
		!(!b.Config.ForceLegacy && b.System.IsEFI()) &&
		!system.IsEmptyDir(b.BootDirectory())
}

// MountBoot mount's the boot partition. The boolean return value indicates that
// the boot partition was mounted by this function.
func (b *BootManager) MountBoot() (bool, error) {
	dev, err := b.BootDevice()
	if err != nil {
		return false, fmt.Errorf("cannot resolve boot device: %w", err)
	}

	// Check if mounted in target destination
	target := b.BootDirectory()
	if b.System.IsMounted(target) || b.IsBootPartitionLess() {
		log.Printf("Default boot partition already mounted or no mount needed")
		return false, nil
	}

	// Check if mounted somewhere else
	if mountPoint, err := b.System.GetMountPointForDevice(dev); err == nil {
		log.Printf("Default boot partition already mounted at %q", mountPoint)

		// Update mount boot directory in config
		b.Config.BootDir = mountPoint

		return false, nil
	}

	// Create boot directory if needed
	if _, err := os.Stat(target); errors.Is(err, fs.ErrNotExist) {
		err = os.MkdirAll(target, 0755)
		if err != nil {
			return false, fmt.Errorf("could not create boot directory: %w", err)
		}
	}

	// Get FS type
	devInfo, err := b.System.GetBlockDevice(dev)
	if err != nil {
		return false, fmt.Errorf("could not determine FS type: %w", err)
	}

	log.Printf("Mounting %q at %q, type %q", dev, target, devInfo.FSType)
	b.MountedBoot = true
	return b.MountedBoot, b.System.Mount(dev, target, devInfo.FSType)
}

// UnmountBoot mount's the boot partition.
func (b *BootManager) UnmountBoot() error {
	log.Printf("Unmounting %q", b.BootDirectory())
	return b.System.Unmount(b.BootDirectory())
}
