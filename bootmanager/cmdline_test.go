package bootmanager

import (
	"path"
	"testing"
)

var cmdLineFileTests = map[string]string{
	"comments":    "init=/bin/bash",
	"mangledmess": "init=/bin/bash rw i8042.nomux thing=off",
	"multi":       "one two three",
	"oneline":     "a single line command line file",
}

func TestParseCmdLineFile(t *testing.T) {
	for file, exp := range cmdLineFileTests {
		l, err := ParseCmdLineFile(path.Join("../fixtures/cmdline/", file))
		if err != nil {
			t.Errorf("error parsing cmdline file %q: %s", file, err)
			continue
		}
		if l != exp {
			t.Errorf("incorrect cmdline: expected %q, got %q", exp, l)
		}
	}
}

var cmdLineDirTests = map[string]string{
	"":              "this is an example of split cmdline handling",
	"vendor_only":   "this is an example of vendor only cmdline handling",
	"vendor_merged": "this goes before /etc/ <hi from cmdline> and this one goes last. :) overridden with mask",
}

func TestParseCmdLineDir(t *testing.T) {
	for dir, exp := range cmdLineDirTests {
		l, err := ParseCmdLineDir(
			path.Join("../fixtures/cmdline/", dir, "usr/share/kernel"),
			path.Join("../fixtures/cmdline/", dir, "etc/kernel"),
		)

		if err != nil {
			t.Errorf("error parsing cmdline dir %q: %s", dir, err)
			continue
		}
		if l != exp {
			t.Errorf("incorrect cmdline: expected \n%q, got\n%q", exp, l)
		}
	}
}

var cmdLineDeleteTests = map[string]struct{ in, out string }{
	"delete_middle": {
		in:  "pre init=/bin/bash foobar rw i8042.nomux thing=off one two three a single line command line file post",
		out: "pre post",
	},
	"delete_ends": {
		in:  "one two three four",
		out: "two three",
	},
	"delete_all": {
		in:  "init=/bin/bash foobar rw i8042.nomux thing=off one two three a single line command line file",
		out: "",
	},
}

func TestParseCmdLineRemovalDir(t *testing.T) {
	for dir, exp := range cmdLineDeleteTests {
		l, err := ParseCmdLineRemovalDir(
			exp.in,
			path.Join("../fixtures/cmdline/", dir, "usr/share/kernel"),
			path.Join("../fixtures/cmdline/", dir, "etc/kernel"),
		)

		if err != nil {
			t.Errorf("error parsing cmdline dir %q: %s", dir, err)
			continue
		}
		if l != exp.out {
			t.Errorf("incorrect cmdline: expected \n%q, got\n%q", exp.out, l)
		}
	}
}
