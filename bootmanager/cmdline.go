package bootmanager

import (
	"errors"
	"io/fs"
	"os"
	"path"
	"path/filepath"
	"sort"
	"strings"
)

const (
	// CmdLineFile contains the name of a single cmdline file.
	CmdLineFile = "cmdline"

	// CmdLineDir contains the name of the directory containing multiple cmdline files.
	CmdLineDir = "cmdline.d"

	// CmdLineRemovalDir contains the name of the directory containing multiple
	// cmdline files to remove from the cmdline.
	CmdLineRemovalDir = "cmdline-removal.d"

	// CmdLineExt contains the extension of modular cmdline config files.
	CmdLineExt = ".conf"
)

// ParseCmdLineDir parses a list of directories containing either a `cmdline` file
// or a `cmdline.d` directory containing `.conf` files.
// `.conf` files from later directories may overrule earlier ones if the filenames match.
func ParseCmdLineDir(dirs ...string) (string, error) {
	files, err := getCmdLineFiles(dirs)
	if err != nil {
		return "", err
	}

	var parts []string
	for _, file := range selectFiles(files) {
		entries, err := parseCmdLineFile(file)
		if err != nil {
			return "", err
		}

		parts = append(parts, entries...)
	}

	return strings.Join(parts, " "), nil
}

// ParseCmdLineRemovalDir parses a list of directories containing a `cmdline-removal.d` directory containing `.conf` files.
// `.conf` files from later directories may overrule earlier ones if the filenames match.
func ParseCmdLineRemovalDir(cmdline string, dirs ...string) (string, error) {
	files, err := getCmdLineRemovals(dirs)
	if err != nil {
		return "", err
	}

	var removals []string
	for _, file := range selectFiles(files) {
		entries, err := parseCmdLineFile(file)
		if err != nil {
			return "", err
		}

		removals = append(removals, entries...)
	}

	var parts []string
	for _, part := range strings.Split(cmdline, " ") {
		found := false
		for _, remove := range removals {
			if part == remove {
				found = true
				break
			}
		}

		if !found {
			parts = append(parts, part)
		}
	}

	return strings.Join(parts, " "), nil
}

// selectFiles selects the files from a file list so files with the same filename
// will only occur once. Later entries will override earlier ones.
func selectFiles(list []string) (selected []string) {
	for _, name := range list {
		// Find previously added files with the same filename, and remove them.
		for i, sel := range selected {
			if path.Base(name) == path.Base(sel) {
				selected = append(selected[:i], selected[i+1:]...)
			}
		}

		selected = append(selected, name)
	}

	return
}

// getCmdLineFiles returns a list of all cmdline files in a list of directories.
func getCmdLineFiles(dirs []string) (files []string, err error) {
	for _, dir := range dirs {
		files, err = findCmdLineFile(files, dir)
		if err != nil {
			return
		}

		files, err = findCmdLineDirEntries(files, path.Join(dir, CmdLineDir))
		if err != nil {
			return
		}
	}

	return
}

// getCmdLineRemovals returns a list of all cmdline removal files in a list of directories.
func getCmdLineRemovals(dirs []string) (files []string, err error) {
	for _, dir := range dirs {
		files, err = findCmdLineDirEntries(files, path.Join(dir, CmdLineRemovalDir))
		if err != nil {
			return
		}
	}

	return
}

// findCmdLineFile checks if the given cmdline file exists and appends the filename if it does.
func findCmdLineFile(files []string, dir string) ([]string, error) {
	file := path.Join(dir, CmdLineFile)
	if _, err := os.Stat(file); err != nil {
		if errors.Is(err, fs.ErrNotExist) {
			return files, nil
		}
		return nil, err
	}

	return append(files, file), nil
}

// findCmdLineDirEntries checks if the given cmdline directory exists,
// and appends its entries if it does.
func findCmdLineDirEntries(files []string, dir string) ([]string, error) {
	list, err := filepath.Glob(path.Join(dir, "*"+CmdLineExt))
	if err != nil {
		if errors.Is(err, fs.ErrNotExist) {
			return files, nil
		}
		return nil, err
	}

	sort.Strings(list)
	return append(files, list...), nil
}

// ParseCmdLineFile parses a single cmdline config file and returns the cmdline string.
func ParseCmdLineFile(path string) (string, error) {
	parts, err := parseCmdLineFile(path)
	if err != nil {
		return "", err
	}

	return strings.Join(parts, " "), nil
}

// parseCmdLineFile parses a single cmdline config file and returns the cmdline string.
func parseCmdLineFile(path string) ([]string, error) {
	data, err := os.ReadFile(path)
	if err != nil {
		return nil, err
	}

	var parts []string
	for _, line := range strings.Split(string(data), "\n") {
		line = strings.Trim(line, " \t\r\n")
		if line == "" || strings.HasPrefix(line, "#") {
			continue
		}
		for _, part := range strings.Split(line, " ") {
			if strings.HasPrefix(part, "#") {
				break
			}
			if part != "" {
				parts = append(parts, part)
			}
		}
	}

	return parts, nil
}
