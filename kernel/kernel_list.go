package kernel

import "sort"

// List represents a kernel list that implements sort.Interface.
type List []*Kernel

// Len is the number of elements in the collection.
func (s List) Len() int {
	return len(s)
}

// Less reports whether the element with index i
// must sort before the element with index j.
func (s List) Less(i, j int) bool {
	if s[i].Type == s[j].Type {
		return s[i].Release > s[j].Release
	}
	return s[i].Type < s[j].Type
}

// Swap swaps the elements with indexes i and j.
func (s List) Swap(i, j int) {
	s[i], s[j] = s[j], s[i]
}

// Contains checks if the kernel is contained in a list of kernels.
func (s List) Contains(kernel *Kernel) bool {
	for _, k := range s {
		if k.Name() == kernel.Name() {
			return true
		}
	}
	return false
}

// Latest returns the latest Kernel version of each Type of Kernel
func (s List) Latest() (latest List) {
	lastType := ""
	sort.Sort(s)
	for _, kernel := range s {
		if kernel.Type != lastType {
			lastType = kernel.Type
			latest = append(latest, kernel)
		}
	}
	return
}
