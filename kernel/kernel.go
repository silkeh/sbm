package kernel

import (
	"errors"
	"fmt"
	"io/fs"
	"os"
	"path"
	"regexp"
	"strconv"
	"strings"
)

var (
	kernelReleaseRegex = regexp.MustCompile(
		`^(?P<version>\d+\.\d+\.\d+)-(?P<rel>\d+)\.(?P<type>\w+)$`)
	kernelFileRegex = regexp.MustCompile(
		`^(?P<namespace>[\w.-]+)\.(?P<type>\w+)\.(?P<version>\d+\.\d+\.\d+)-(?P<rel>\d+)$`)
)

const (
	// CmdLinePrefix contains the prefix of the Linux cmdline file.
	CmdLinePrefix = "cmdline"

	// ConfigPrefix contains the prefix of the Linux config file.
	ConfigPrefix = "config"

	// InitrdPrefix contains the prefix of the initrd file.
	InitrdPrefix = "initrd"

	// SysMapPrefix contains the prefix of the system map file.
	SysMapPrefix = "System.map"
)

// Kernel represents an installable or running kernel.
type Kernel struct {
	Namespace   string // Distribution namespace.
	Version     string // Version string (eg: 5.10.4).
	Release     int    // Release number.
	Type        string // Kernel type (eg: current or lts).
	Directory   string // Directory containing the kernel.
	SysCmdLine  string // System command line string.
	UserCmdLine string // User command line string.
}

// New returns a Kernel based on a provided kernel release.
// The release must be in the format returned by `uname -r`: `<version>-<release>.<type>`
func New(namespace, kernelRelease, dir string) (*Kernel, error) {
	_, v, t, r, err := parseVersionRegex(kernelReleaseRegex, kernelRelease)
	if err != nil {
		return nil, err
	}

	return &Kernel{
		Namespace: namespace,
		Version:   v,
		Release:   r,
		Type:      t,
		Directory: dir,
	}, nil
}

// FromLoaderEntry returns a Kernel based on its name.
// The name must be in the format `<namespace>-<type>-<version>-<release>.conf`
func FromLoaderEntry(kernelNameSpace, dir, name string) (*Kernel, error) {
	parts := strings.SplitN(strings.TrimSuffix(path.Base(name), ".conf"), "-", 4)
	if len(parts) != 4 {
		return nil, fmt.Errorf("invalid kernel name: %s", name)
	}

	rel, err := strconv.Atoi(parts[3])
	return &Kernel{
		Namespace: kernelNameSpace,
		Version:   parts[2],
		Release:   rel,
		Type:      parts[1],
		Directory: dir,
	}, err
}

// FromPath initializes a kernel from a filesystem path.
// The name must be in the format `<namespace>.<type>.<version>-<release>`
func FromPath(dir, name string) (k *Kernel, err error) {
	if hasAnyPrefix(name, CmdLinePrefix, ConfigPrefix, InitrdPrefix, SysMapPrefix) {
		err = errors.New("not a kernel")
		return
	}

	n, v, t, r, err := parseVersionRegex(kernelFileRegex, name)
	if err != nil {
		return nil, err
	}

	return &Kernel{
		Namespace: n,
		Version:   v,
		Release:   r,
		Type:      t,
		Directory: dir,
	}, nil
}

// String returns a human-readable description of the kernel.
func (k *Kernel) String() string {
	return k.Name()
}

// Name returns the name of the kernel in the form: <namespace>.<type>.<version>-<release>
func (k *Kernel) Name() string {
	return fmt.Sprintf("%s.%s.%s-%v", k.Namespace, k.Type, k.Version, k.Release)
}

// KernelRelease returns the kernel in the form reported by `uname`: <version>-<release>.<type>
func (k *Kernel) KernelRelease() string {
	return fmt.Sprintf("%s-%v.%s", k.Version, k.Release, k.Type)
}

// KernelPath returns the full path to the kernel file.
func (k *Kernel) KernelPath() string {
	return path.Join(k.Directory, k.Name())
}

// InitrdPath returns the full path to the initrd file.
func (k *Kernel) InitrdPath() string {
	return path.Join(k.Directory, InitrdPrefix+"-"+k.Name())
}

// CmdlinePath returns the full path to the kernel cmdline file.
func (k *Kernel) CmdlinePath() string {
	return path.Join(k.Directory, CmdLinePrefix+"-"+k.KernelRelease())
}

// ConfigPath returns the full path to the kernel config file.
func (k *Kernel) ConfigPath() string {
	return path.Join(k.Directory, ConfigPrefix+"-"+k.KernelRelease())
}

// SysMapPath returns the full path to the system map file.
func (k *Kernel) SysMapPath() string {
	return path.Join(k.Directory, SysMapPrefix+"-"+k.KernelRelease())
}

// CmdLine assembles the command line using the system, kernel and user cmdline entries.
func (k *Kernel) CmdLine() (string, error) {
	data, err := os.ReadFile(k.CmdlinePath())
	if err != nil {
		return "", err
	}

	return k.SysCmdLine + " " + strings.Trim(string(data), " \r\n") + " " + k.UserCmdLine, nil
}

// Remove removes the kernel from the system.
// Note that header files are not removed.
func (k *Kernel) Remove() error {
	files := []string{k.KernelPath(), k.InitrdPath(), k.CmdlinePath(), k.ConfigPath(), k.SysMapPath()}
	for _, f := range files {
		err := os.Remove(f)
		if err != nil && !errors.Is(err, fs.ErrNotExist) {
			return err
		}
	}

	return nil
}

// parseVersionRegex parses a string to the version components using the given regex.
func parseVersionRegex(regex *regexp.Regexp, str string) (n, v, t string, r int, err error) {
	match := regex.FindStringSubmatch(str)
	if len(match) != regex.NumSubexp()+1 {
		err = fmt.Errorf("invalid  %q", str)
		return
	}

	for i, name := range regex.SubexpNames() {
		switch name {
		case "namespace":
			n = match[i]
		case "version":
			v = match[i]
		case "rel":
			r, err = strconv.Atoi(match[i])
		case "type":
			t = match[i]
		}
	}

	return
}
