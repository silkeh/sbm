package kernel

import "testing"

var (
	unsortedKernelList = List{
		solusLTS175,
		solusCurrent201,
		solusCurrent202,
		solusLTS178,
	}

	latestKernelList = List{
		solusCurrent202,
		solusLTS178,
	}
)

func TestList_Contains(t *testing.T) {
	newKernel := &Kernel{
		Directory: "/doesnotexist",
		Type:      "old",
		Namespace: "common.Solus-project",
		Version:   "1.2.3",
		Release:   123,
	}
	tests := map[*Kernel]bool{
		solusLTS178: true,
		newKernel:   false,
	}

	for k, exp := range tests {
		res := unsortedKernelList.Contains(k)
		if res != exp {
			t.Errorf("kernel list contains %q: expected %v, got %v", k, exp, res)
		}
	}
}

func TestList_Latest(t *testing.T) {
	latest := unsortedKernelList.Latest()
	for i, kernel := range latest {
		if latestKernelList[i].Name() != kernel.Name() {
			t.Errorf("Latest returned incorrect kernel: expected %q, got %q", latestKernelList, kernel)
		}
	}
}
