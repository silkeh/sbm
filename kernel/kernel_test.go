package kernel

import (
	"errors"
	"io/fs"
	"os"
	"path"
	"testing"
)

var (
	ridiculous = []string{
		"0",
		"",
		"4.30",
		".-",
		".",
		"@",
		"@!_+",
		"4.4.0-",
		".0-",
		".-lts",
		"0.-lts",
		"4.0.20-190.",
		CmdLinePrefix,
		ConfigPrefix,
		InitrdPrefix,
		SysMapPrefix,
	}
	solusCurrent202 = &Kernel{
		Directory: "/usr/lib/kernel",
		Namespace: "com.solus-project",
		Version:   "5.14.14",
		Release:   202,
		Type:      "current",
	}
	solusLTS178 = &Kernel{
		Directory: "/usr/lib/kernel",
		Namespace: "com.solus-project",
		Version:   "4.14.246",
		Release:   178,
		Type:      "lts",
	}
	solusCurrent201 = &Kernel{
		Directory: "/usr/lib/kernel",
		Namespace: "com.solus-project",
		Version:   "5.14.12",
		Release:   201,
		Type:      "current",
	}
	solusLTS175 = &Kernel{
		Directory: "/usr/lib/kernel",
		Namespace: "com.solus-project",
		Version:   "4.14.237",
		Release:   175,
		Type:      "lts",
	}
)

func TestFromLoaderEntry(t *testing.T) {
	for _, name := range ridiculous {
		_, err := FromLoaderEntry("namespace", "/test", name)
		if err == nil {
			t.Errorf("LoaderEntry parsed `%q` as a valid kernel name (expected error)", name)
		}
	}
	tests := map[*Kernel]string{
		solusCurrent202: "Solus-current-5.14.14-202.conf",
		solusLTS178:     "Solus-lts-4.14.246-178.conf",
	}

	for exp, name := range tests {
		kernel, err := FromLoaderEntry("com.solus-project", "/usr/lib/kernel", name)
		if err != nil {
			t.Errorf("FromLoaderEntry returned error on %q: %s", name, err)
			continue
		}
		if kernel.Name() != exp.Name() {
			t.Errorf("FromLoaderEntry returned incorrect name: expected %q, got %q", kernel.Name(), exp.Name())
		}
	}
}

func TestFromPath(t *testing.T) {
	for _, name := range ridiculous {
		_, err := FromPath("/test", name)
		if err == nil {
			t.Errorf("FromPath parsed `%q` as a valid kernel name (expected error)", name)
		}
	}
	tests := map[*Kernel]string{
		solusCurrent202: "com.solus-project.current.5.14.14-202",
		solusLTS178:     "com.solus-project.lts.4.14.246-178",
	}

	for exp, name := range tests {
		kernel, err := FromPath("/usr/lib/kernel", name)
		if err != nil {
			t.Errorf("FromPath returned error on %q: %s", name, err)
			continue
		}
		if kernel.Name() != exp.Name() {
			t.Errorf("FromPath returned incorrect name: expected %q, got %q", kernel.Name(), exp.Name())
		}
	}
}

func TestNew(t *testing.T) {
	for _, name := range ridiculous {
		_, err := New("namespace", name, "/test")
		if err == nil {
			t.Errorf("New parsed `%q` as a valid kernel name (expected error)", name)
		}
	}

	tests := map[*Kernel]string{
		solusCurrent202: "5.14.14-202.current",
		solusLTS178:     "4.14.246-178.lts",
	}

	for exp, release := range tests {
		kernel, err := New("com.solus-project", release, "/usr/lib/kernel")
		if err != nil {
			t.Errorf("New returned error on %q: %s", release, err)
			continue
		}
		if kernel.KernelRelease() != release {
			t.Errorf("New returned incorrect release: expected %q, got %q", release, kernel.KernelRelease())
		}
		if exp.Name() != kernel.Name() {
			t.Errorf("New returned incorrect name: expected %q, got %q", exp.Name(), kernel.Name())
		}
	}
}

func TestKernel_KernelPath(t *testing.T) {
	tests := map[*Kernel]string{
		solusCurrent201: "/usr/lib/kernel/com.solus-project.current.5.14.12-201",
		solusLTS178:     "/usr/lib/kernel/com.solus-project.lts.4.14.246-178",
	}
	for kernel, p := range tests {
		result := kernel.KernelPath()
		if p != result {
			t.Errorf("incorrect kernel p: expected %q, got %q", p, result)
		}
	}
}

func TestKernel_Remove(t *testing.T) {
	type testCase struct {
		files   []string
		remains []string
	}

	// Specify the different tests
	tests := map[*Kernel]testCase{
		solusLTS178: {
			files: []string{
				"com.solus-project.lts.4.14.246-178",
				"initrd-com.solus-project.lts.4.14.246-178",
				"cmdline-4.14.246-178.lts",
				"config-4.14.246-178.lts",
				"System.map-4.14.246-178.lts",
			},
			remains: []string{},
		},
		// Missing one file, must not error
		solusCurrent201: {
			files: []string{
				"com.solus-project.current.5.14.12-201",
				"initrd-com.solus-project.current.5.14.12-201",
				"config-5.14.12-201.current",
				"System.map-5.14.12-201.current",
			},
			remains: []string{},
		},
		// Having one extra file, must not remove extra file
		solusCurrent202: {
			files: []string{
				"com.solus-project.current.5.14.14-202",
				"com.Solus-project.current.5.14.14-202",
				"initrd-com.solus-project.current.5.14.14-202",
				"cmdline-5.14.14-202.current",
				"config-5.14.14-202.current",
				"config-5.14.12-201.current",
				"System.map-5.14.14-202.current",
			},
			remains: []string{
				"com.Solus-project.current.5.14.14-202",
				"config-5.14.12-201.current",
			},
		},
	}

	// Create a temp dir that can be easily cleaned up using defer.
	temp, err := os.MkdirTemp("", "kernel-remove-test")
	if err != nil {
		t.Errorf("failed to create temp dir for test: %s", err)
	}
	defer func() {
		err = os.RemoveAll(temp)
		if err != nil {
			t.Errorf("failed to remove %q: %s", temp, err)
		}
	}()

	for k, test := range tests {
		k.Directory, err = os.MkdirTemp(temp, k.String())
		if err != nil {
			t.Errorf("failed to create temp dir for test: %s", err)
		}
		// create all files for current kernel
		for _, file := range test.files {
			err = os.WriteFile(path.Join(k.Directory, file), nil, 0644)
			if err != nil {
				t.Errorf("failed to create temp file for test: %s", err)
			}
		}
		// clean up kernel
		err = k.Remove()
		if err != nil {
			t.Errorf("kernel could not be removed: %s", err)
		}
		// what remains?
		for _, file := range test.remains {
			err = os.Remove(path.Join(k.Directory, file))
			if errors.Is(err, fs.ErrNotExist) {
				t.Errorf("expected to find file %q, but it was not there", file)
			} else if err != nil {
				t.Errorf("failed to clean up file %q: %s", file, err)
			}
		}
		// folder must be empty
		remainingFiles, err := os.ReadDir(k.Directory)
		if err != nil {
			t.Errorf("failed to read temp dir: %s", err)
		}
		if len(remainingFiles) != 0 {
			t.Error("failed to remove all kernel files")
		}
	}
}
