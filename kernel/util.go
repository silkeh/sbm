package kernel

import "strings"

func hasAnyPrefix(str string, prefix ...string) bool {
	for _, p := range prefix {
		if strings.HasPrefix(str, p) {
			return true
		}
	}

	return false
}
