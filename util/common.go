package util

import (
	"bytes"
	"encoding/json"
	"fmt"
	"os/exec"
)

func runJSON(cmd *exec.Cmd, v interface{}) error {
	var out, errOut bytes.Buffer
	cmd.Stdout = &out
	cmd.Stderr = &errOut

	err := cmd.Run()
	if err != nil {
		return fmt.Errorf("%s (%w)", errOut.String(), err)
	}

	err = json.Unmarshal(out.Bytes(), v)
	if err != nil {
		return fmt.Errorf("error parsing output: %w", err)
	}

	return nil
}
