package util

import (
	"os/exec"
)

// lsblkResult represents an lsblk JSON result.
type lsblkResult struct {
	BlockDevices []*BlockDevice `json:"blockdevices"`
}

// BlockDevice represents a block device.
type BlockDevice struct {
	Name               string         `json:"name"`
	KName              string         `json:"kname"`
	Path               string         `json:"path"`
	MajMin             string         `json:"maj:min"`
	FSAvail            string         `json:"fsavail"`
	FSSize             string         `json:"fssize"`
	FSType             string         `json:"fstype"`
	FSUsed             string         `json:"fsused"`
	FSUsePercent       string         `json:"fsuse%"`
	FSVersion          string         `json:"fsver"`
	MountPoint         string         `json:"mountpoint"`
	Label              string         `json:"label"`
	UUID               string         `json:"uuid"`
	PTUUID             string         `json:"ptuuid"`
	PTType             string         `json:"pttype"`
	PartType           string         `json:"parttype"`
	PartTypeName       string         `json:"parttypename"`
	PartLabel          string         `json:"partlabel"`
	PartUUID           string         `json:"partuuid"`
	PartFlags          string         `json:"partflags"`
	ReadAhead          int            `json:"ra"`
	ReadOnly           bool           `json:"ro"`
	Removable          bool           `json:"rm"`
	HotPlug            bool           `json:"hotplug"`
	Model              string         `json:"model"`
	Serial             string         `json:"serial"`
	Size               string         `json:"size"`
	State              string         `json:"state"`
	Owner              string         `json:"owner"`
	Group              string         `json:"group"`
	Mode               string         `json:"mode"`
	Alignment          int            `json:"alignment"`
	MinIO              int            `json:"min-io"`
	OptIO              int            `json:"opt-io"`
	PhysicalSectors    int            `json:"phy-sec"`
	LogicalSectors     int            `json:"log-sec"`
	Rotational         bool           `json:"rota"`
	Scheduler          string         `json:"sched"`
	RQSize             int            `json:"rq-size"`
	Type               string         `json:"type"`
	DiscardAlignment   int            `json:"disc-aln"`
	DiscardGranularity string         `json:"disc-gran"`
	DiscardMax         string         `json:"disc-max"`
	DiscardZero        bool           `json:"disc-zero"`
	WriteSame          string         `json:"wsame"`
	WWN                string         `json:"wwn"`
	Rand               bool           `json:"rand"`
	PKName             string         `json:"pkname"`
	HCTL               string         `json:"hctl"`
	Transport          string         `json:"tran"`
	Subsystems         string         `json:"subsystems"`
	Revision           string         `json:"rev"`
	Vendor             string         `json:"vendor"`
	Zoned              string         `json:"zoned"`
	DAX                bool           `json:"dax"`
	Children           []*BlockDevice `json:"children,omitempty"`
}

// BlockDevices returns information on block devices via `lsblk`.
func BlockDevices(dev ...string) ([]*BlockDevice, error) {
	cmd := exec.Command("lsblk", append([]string{"-OJ"}, dev...)...)
	res := new(lsblkResult)
	err := runJSON(cmd, &res)
	if err != nil {
		return nil, err
	}

	return res.BlockDevices, nil
}
