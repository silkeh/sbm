package util

import "os/exec"

// findmntResult contains a findmnt result.
type findmntResult struct {
	Filesystems []*Filesystem `json:"filesystems"`
}

// Filesystem represents a Linux filesystem.
type Filesystem struct {
	Source      string       `json:"source"`
	Target      string       `json:"target"`
	FStype      string       `json:"fstype"`
	Options     string       `json:"options"`
	VFSOptions  string       `json:"vfs-options"`
	FSOtions    string       `json:"fs-options"`
	Label       string       `json:"label"`
	UUID        string       `json:"uuid"`
	PartLabel   string       `json:"partlabel"`
	PartUUID    string       `json:"partuuid"`
	MajMin      string       `json:"maj:min"`
	Action      string       `json:"action"`
	OldTarget   string       `json:"old-target"`
	OldOptions  string       `json:"old-options"`
	Size        string       `json:"size"`
	Available   string       `json:"avail"`
	Used        string       `json:"used"`
	UsePercent  string       `json:"use%"`
	FSroot      string       `json:"fsroot"`
	TID         int          `json:"tid"`
	ID          int          `json:"id"`
	OptFields   string       `json:"opt-fields"`
	Propagation string       `json:"propagation"`
	Freq        string       `json:"freq"`
	PassNo      string       `json:"passno"`
	Children    []Filesystem `json:"children,omitempty"`
}

// Filesystems returns information on filesystems via `findmnt`.
func Filesystems(target ...string) ([]*Filesystem, error) {
	cmd := exec.Command("findmnt", append([]string{"--output-all", "--json"}, target...)...)
	res := new(findmntResult)
	err := runJSON(cmd, &res)
	if err != nil {
		return nil, err
	}

	return res.Filesystems, nil
}
