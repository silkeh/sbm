DESTDIR     := /
BIN_DIR     := $(DESTDIR)/usr/bin
SYSTEMD_DIR := $(DESTDIR)/usr/lib/systemd/system

GEN_FILES := config/config.go config/version.go
GO_FILES  := $(shell find */ -name "*.go")
GO_ARGS   := -ldflags="-s -w"

SERVICE_DIR := dist/systemd
SERVICES    := $(addprefix $(SYSTEMD_DIR)/,$(notdir $(wildcard $(SERVICE_DIR)/*.service)))

all: sbm

generate:
	go generate ./...

debug: GO_ARGS = -race
debug: sbm

sbm: $(GO_FILES) $(GEN_FILES)
	go build $(GO_ARGS) ./cmd/sbm

$(GEN_FILES):
ifeq (, $(wildcard $(GEN_FILES)))
$(GEN_FILES): generate
endif

test: generate fixtures
	go test -cover ./...

fixtures: fixtures/.extracted

%/.extracted: %.ttar
	./tools/ttar/ttar -x -f $<
	touch $@

update-fixtures:
	rm -f fixtures/.extracted
	./tools/ttar/ttar -c -f fixtures.ttar fixtures

install: $(BIN_DIR)/sbm $(SERVICES)

$(BIN_DIR)/sbm: sbm
	install -Dm0755 $< $@

$(SERVICES): $(SYSTEMD_DIR)/%: $(SERVICE_DIR)/%
	install -Dm0644 $< $@

clean:
	rm -f sbm $(GEN_FILES)

.PHONY: debug release generate test fixtures update-fixtures install clean
