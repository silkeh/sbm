package bootloader

// Capabilities represents bootloader capabilities.
type Capabilities struct {
	UEFI          bool // Supports UEFI
	Legacy        bool // Supports Legacy boot (MBR).
	ExtFS         bool // Supports Ext filesystems (for legacy boot).
	FATFS         bool // Supports FAT filesystems.
	PartitionLess bool // Supports (legacy) boots without a boot partition.
}
