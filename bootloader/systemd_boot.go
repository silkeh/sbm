package bootloader

import (
	"errors"
	"fmt"
	"io/fs"
	"os"
	"path"
	"path/filepath"

	"gitlab.com/silkeh/sbm/kernel"
	"gitlab.com/silkeh/sbm/system"
)

const (
	// SystemdBootName contains the name of the systemd bootloader.
	SystemdBootName = "systemd-bootx64.efi"
)

// SystemdBoot implements the systemd-boot bootloader.
type SystemdBoot struct {
	EFI
	SecureBoot *SecureBoot
}

// NewSystemdBoot initializes a systemd boot.
func NewSystemdBoot(config *system.Config, prefix string, secureBoot *SecureBoot) *SystemdBoot {
	return &SystemdBoot{
		EFI: EFI{
			Config: config,
			Prefix: prefix,
		},
		SecureBoot: secureBoot,
	}
}

// Name returns the name of the bootloader.
func (s *SystemdBoot) Name() string {
	return "systemd-boot"
}

// Capabilities returns the capabilities of the bootloader.
func (s *SystemdBoot) Capabilities() *Capabilities {
	return &Capabilities{
		UEFI:  true,
		FATFS: true,
	}
}

// GetTimeout returns the current boot menu timeout.
func (s *SystemdBoot) GetTimeout() (int, error) {
	loader, err := NewLoaderConfig(s.Config.BootDir)
	if err != nil {
		return 0, err
	}
	return loader.Timeout, nil
}

// SetTimeout sets the current boot menu timeout.
func (s *SystemdBoot) SetTimeout(t int) error {
	loader, err := NewLoaderConfig(s.Config.BootDir)
	if err != nil {
		return err
	}
	loader.Timeout = t
	return loader.Write()
}

// GetDefaultKernel returns the default kernel.
func (s *SystemdBoot) GetDefaultKernel() (*kernel.Kernel, error) {
	loader, err := NewLoaderConfig(s.Config.BootDir)
	if err != nil {
		return nil, err
	}
	// Check for new name
	k, err := s.kernel(loader.DefaultBoot)
	if err == nil {
		return k, nil
	}

	// Check for non-unified name
	return kernel.FromLoaderEntry(s.Config.KernelNamespace, s.Config.KernelDir, loader.DefaultBoot)
}

// SetDefaultKernel sets the default kernel.
// Note that this doesn't have to be installed.
func (s *SystemdBoot) SetDefaultKernel(kernel *kernel.Kernel) error {
	loader, err := NewLoaderConfig(s.Config.BootDir)
	if err != nil {
		return err
	}
	loader.DefaultBoot = kernel.Name() + ".efi"
	return loader.Write()
}

// entryConfPath returns the systemd-boot config path for a kernel.
func (s *SystemdBoot) entryConfPath(k *kernel.Kernel) string {
	file := fmt.Sprintf("%s-%s-%s-%v.conf", s.Config.VendorPrefix, k.Type, k.Version, k.Release)
	return path.Join(s.Config.BootDir, "loader/entries", file)
}

// InstallKernel installs a kernel.
func (s *SystemdBoot) InstallKernel(k *kernel.Kernel) error {
	return s.SecureBoot.BundleAndSign(
		k, s.OSReleasePath(), s.LinuxEFIStubPath(),
		s.installPath(k))
}

// RemoveKernel removes a kernel.
func (s *SystemdBoot) RemoveKernel(k *kernel.Kernel) error {
	// Check and remove non-unified kernel
	if err := s.removeNonUnifiedKernel(k); err != nil {
		return err
	}

	// Check if kernel exists
	if _, err := os.Stat(s.installPath(k)); err != nil {
		if errors.Is(err, fs.ErrNotExist) {
			return nil
		}
		return err
	}

	return os.Remove(s.installPath(k))
}

func (s *SystemdBoot) removeNonUnifiedKernel(k *kernel.Kernel) error {
	files := []string{
		path.Join(s.Config.BootDir, "EFI", s.Config.KernelNamespace, "kernel-"+k.Name()),
		path.Join(s.Config.BootDir, "EFI", s.Config.KernelNamespace, "initrd-"+k.Name()),
		s.entryConfPath(k),
	}

	for _, f := range files {
		err := os.Remove(f)
		if err != nil && !errors.Is(err, fs.ErrNotExist) {
			return err
		}
	}

	return nil
}

// ListKernels returns a list of all installed kernels.
func (s *SystemdBoot) ListKernels() ([]*kernel.Kernel, error) {
	kernels := make([]*kernel.Kernel, 0, 16)

	// Get unified kernels
	matches, err := filepath.Glob(path.Join(s.Config.BootDir, "EFI/Linux", s.Config.KernelNamespace+".*.efi"))
	if err != nil {
		return nil, err
	}
	for _, m := range matches {
		k, err := s.EFI.kernel(m)
		if err == nil {
			kernels = append(kernels, k)
		}
	}

	// Get old-style kernels
	matches, err = filepath.Glob(path.Join(s.Config.BootDir, "loader/entries", s.Config.VendorPrefix+"*.conf"))
	if err != nil {
		return nil, err
	}
	for _, m := range matches {
		k, err := kernel.FromLoaderEntry(s.Config.KernelNamespace, s.Config.KernelDir, m)
		if err == nil {
			kernels = append(kernels, k)
		}
	}

	return kernels, nil
}

// NeedsInstall returns true if the bootloader needs a (re)install.
func (s *SystemdBoot) NeedsInstall() bool {
	if _, err := os.Stat(s.BootLoaderDestination()); err != nil {
		return true
	}

	if _, err := os.Stat(s.FallbackPath()); err != nil {
		return true
	}

	return false
}

// NeedsUpdate returns true if the bootloader needs to be updated.
func (s *SystemdBoot) NeedsUpdate() bool {
	ok, _ := s.SecureBoot.Verify(s.BootLoaderSource(), s.BootLoaderDestination())
	if !ok {
		return true
	}

	ok, _ = s.SecureBoot.Verify(s.BootLoaderSource(), s.FallbackPath())
	return !ok
}

// Install installs the bootloader.
func (s *SystemdBoot) Install() error {
	// Create required directories
	for _, d := range []string{s.VendorDir(), s.TargetDir()} {
		if err := os.MkdirAll(d, 0755); err != nil {
			return err
		}
	}

	// Install files
	return s.Update()
}

// Update updates the bootloader.
func (s *SystemdBoot) Update() error {
	err := os.MkdirAll(s.VendorDir(), 0755)
	if err != nil {
		return err
	}

	err = s.SecureBoot.Sign(s.BootLoaderSource(), s.BootLoaderDestination())
	if err != nil {
		return err
	}

	return s.SecureBoot.Sign(s.BootLoaderSource(), s.FallbackPath())
}

// Remove uninstalls the bootloader.
func (s *SystemdBoot) Remove() error {
	panic("implement me")
}

// BootLoaderSource returns the source path of the bootloader.
func (s *SystemdBoot) BootLoaderSource() string {
	return path.Join(s.Prefix, SystemdEFILibPath, SystemdBootName)
}

// BootLoaderDestination returns the destination path of the bootloader.
func (s *SystemdBoot) BootLoaderDestination() string {
	return path.Join(s.VendorDir(), SystemdBootName)
}
