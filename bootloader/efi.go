package bootloader

import (
	"path"
	"strings"

	"gitlab.com/silkeh/sbm/kernel"
	"gitlab.com/silkeh/sbm/system"
)

const (
	// FallbackEFIFile contains the path to the fallback EFI bootloader.
	FallbackEFIFile = "EFI/BOOT/BOOTX64.EFI"

	// LinuxEFIStubName contains the name of the Linux EFI stub.
	LinuxEFIStubName = "linuxx64.efi.stub"

	// OSReleasePath contains the path to the OS release file.
	OSReleasePath = "/etc/os-release"

	// SystemdEFILibPath contains the path to the systemd EFI libraries.
	SystemdEFILibPath = "/usr/lib/systemd/boot/efi"
)

// EFI implements the generic EFI components
type EFI struct {
	Config *system.Config
	Prefix string
}

// TargetDir returns the target directory for the kernel.
func (e *EFI) TargetDir() string {
	return path.Join(e.Config.BootDir, "EFI/Linux")
}

// VendorDir returns the bootloader vendor directory.
func (e *EFI) VendorDir() string {
	return path.Join(e.Config.BootDir, "EFI/systemd")
}

func (e *EFI) kernel(name string) (*kernel.Kernel, error) {
	return kernel.FromPath(e.Config.KernelDir, strings.TrimSuffix(path.Base(name), ".efi"))
}

// LinuxEFIStubPath returns the Linux EFI stub path.
func (e *EFI) LinuxEFIStubPath() string {
	return path.Join(e.Prefix, SystemdEFILibPath, LinuxEFIStubName)
}

// OSReleasePath returns the path to the OS release file.
func (e *EFI) OSReleasePath() string {
	return path.Join(e.Prefix, OSReleasePath)
}

// installPath returns the installation path for a kernel.
func (e *EFI) installPath(k *kernel.Kernel) string {
	return path.Join(e.TargetDir(), k.Name()+".efi")
}

// FallbackPath returns the path to the fallback EFI file.
func (e *EFI) FallbackPath() string {
	return path.Join(e.Config.BootDir, FallbackEFIFile)
}
