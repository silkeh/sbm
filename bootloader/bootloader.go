package bootloader

import (
	"gitlab.com/silkeh/sbm/kernel"
)

// Bootloader represents a bootloader.
type Bootloader interface {
	// Name returns the name of the bootloader.
	Name() string

	// Capabilities returns the capabilities of the bootloader.
	Capabilities() *Capabilities

	// GetTimeout returns the current boot menu timeout.
	GetTimeout() (int, error)

	// SetTimeout sets the current boot menu timeout.
	SetTimeout(int) error

	// GetDefaultKernel returns the default kernel.
	GetDefaultKernel() (*kernel.Kernel, error)

	// SetDefaultKernel sets the default kernel.
	// Note that this doesn't have to be installed.
	SetDefaultKernel(*kernel.Kernel) error

	// InstallKernel installs a kernel.
	InstallKernel(*kernel.Kernel) error

	// RemoveKernel removes a kernel.
	RemoveKernel(*kernel.Kernel) error

	// ListKernels returns a list of all installed kernels.
	ListKernels() ([]*kernel.Kernel, error)

	// NeedsInstall returns true if the bootloader needs a (re)install.
	NeedsInstall() bool

	// NeedsUpdate returns true if the bootloader needs to be updated.
	NeedsUpdate() bool

	// Install installs the bootloader.
	Install() error

	// Update updates the bootloader.
	Update() error

	// Remove uninstalls the bootloader.
	Remove() error
}
