package bootloader

import (
	"bytes"
	"crypto/sha256"
	"errors"
	"fmt"
	"io"
	"log"
	"os"
	"path"
	"path/filepath"

	"github.com/foxboron/go-uefi/efi/pecoff"
	"github.com/foxboron/go-uefi/efi/util"
	"github.com/foxboron/sbctl"
	"github.com/google/uuid"

	"gitlab.com/silkeh/sbm/kernel"
	"gitlab.com/silkeh/sbm/system"
)

const (
	// KeyDir returns the directory name containing the secure boot keys.
	KeyDir = "keys"
)

// SecureBoot contains secure boot state.
type SecureBoot struct {
	Config *system.Config
}

// KeyDir returns the directory containing secure boot keys.
func (s *SecureBoot) KeyDir() string {
	return path.Join(s.Config.SecureBootDir, KeyDir)
}

// Initialized returns true if the secure boot keys have been initialized.
func (s *SecureBoot) Initialized() bool {
	return sbctl.CheckIfKeysInitialized(s.KeyDir())
}

// GenerateKeys generates secure boot keys.
func (s *SecureBoot) GenerateKeys() error {
	if err := sbctl.CreateDirectory(s.Config.SecureBootDir); err != nil {
		return err
	}

	log.Printf("SecureBoot: generating UUID")
	if _, err := sbctl.CreateGUID(s.Config.SecureBootDir); err != nil {
		return err
	}

	if !sbctl.CheckIfKeysInitialized(s.KeyDir()) {
		log.Printf("SecureBoot: initializing keys")
		if err := sbctl.InitializeSecureBootKeys(s.KeyDir()); err != nil {
			return err
		}
	}

	log.Printf("SecureBoot: initialized")

	return nil
}

// Sign signs a file for secure boot.
func (s *SecureBoot) Sign(in, out string) error {
	log.Printf("Secureboot: signing %q -> %q", in, out)
	return sbctl.Sign(in, out, false)
}

// CheckSignature verifies a signed file.
func (s *SecureBoot) CheckSignature(path string) (bool, error) {
	return sbctl.VerifyFile(sbctl.DBCert, path)
}

// Verify verifies that a file is signed and identical.
func (s *SecureBoot) Verify(in, out string) (bool, error) {
	ok, err := s.CheckSignature(out)
	if err != nil || !ok {
		return ok, err
	}

	return peEqual(in, out)
}

// peEqual returns true if two EFI executables are equal in content.
func peEqual(in, out string) (bool, error) {
	inHash, err := pecoffChecksum(in)
	if err != nil {
		return false, err
	}

	outHash, err := pecoffChecksum(out)
	if err != nil {
		return false, err
	}

	return bytes.Equal(inHash, outHash), nil
}

// pecoffChecksum returns a checksum for the PE/COFF sections of a file.
func pecoffChecksum(path string) ([]byte, error) {
	data, err := os.ReadFile(path)
	if err != nil {
		return nil, err
	}

	// Hash the data
	hash := sha256.New()
	n, _ := io.Copy(hash, pecoff.PECOFFChecksum(data).SigData)

	// Add padding if needed
	if n%4 != 0 {
		hash.Write(make([]byte, 4-n%4))
	}

	return hash.Sum(nil), err
}

// BundleAndSign bundles a kernel, release file and efi stub in a unified kernel image and signs the result.
func (s *SecureBoot) BundleAndSign(k *kernel.Kernel, releaseFile, efistub, output string) error {
	// Create unified image
	efiFile := path.Join("/tmp", k.Name())
	err := s.Bundle(k, releaseFile, efistub, efiFile)
	if err != nil {
		return err
	}
	defer func() { os.Remove(efiFile) }()

	// Sign unified image and install
	return s.Sign(efiFile, output)
}

// Bundle bundles a kernel, release file and efi stub in a unified kernel image.
func (s *SecureBoot) Bundle(k *kernel.Kernel, releaseFile, efistub, output string) error {
	log.Printf("Secureboot: bundling %q -> %q", k.Name(), output)
	cmdLineFile := path.Join("/tmp", "cmdline-"+k.Name())
	err := s.writeCmdLine(k, cmdLineFile)
	if err != nil {
		return err
	}

	defer func() { os.Remove(cmdLineFile) }()

	return sbctl.CreateBundle(sbctl.Bundle{
		Output:      output,
		KernelImage: k.KernelPath(),
		Initramfs:   k.InitrdPath(),
		Cmdline:     cmdLineFile,
		OSRelease:   releaseFile,
		EFIStub:     efistub,
	})
}

// writeCmdLine writes the kernel command line to a file.
func (s *SecureBoot) writeCmdLine(k *kernel.Kernel, out string) error {
	cmdLine, err := k.CmdLine()
	if err != nil {
		return err
	}

	return os.WriteFile(out, []byte(cmdLine+"\n"), 0644)
}

// Enroll enrolls the local EFI keys to the system certificate store.
func (s *SecureBoot) Enroll() error {
	if err := checkImmutable(); err != nil {
		return err
	}

	guid, err := s.getGUID(path.Join(s.Config.SecureBootDir, "GUID"))
	if err != nil {
		return err
	}

	log.Printf("Enrolling keys")
	err = sbctl.KeySync(*util.StringToGUID(guid.String()), sbctl.KeysPath)
	if err != nil {
		return fmt.Errorf("error syncing keys: %w", err)
	}

	log.Printf("Keys enrolled")
	return nil
}

// EnrollMicrosoft signs and enrolls Microsoft certificates to the system certificate store.
func (s *SecureBoot) EnrollMicrosoft() error {
	guid, err := s.getGUID(path.Join(s.Config.MSSecureBootDir, "GUID"))
	if err != nil {
		return err
	}
	efiGUID := *util.StringToGUID(guid.String())

	KEKKey, _ := os.ReadFile(filepath.Join(s.KeyDir(), "KEK", "KEK.key"))
	KEKPem, _ := os.ReadFile(filepath.Join(s.KeyDir(), "KEK", "KEK.pem"))
	matches, _ := filepath.Glob(path.Join(s.Config.MSSecureBootDir, "*.pem"))
	for _, m := range matches {
		dbPem, _ := os.ReadFile(m)
		if err := sbctl.Enroll(efiGUID, dbPem, KEKKey, KEKPem, "db"); err != nil {
			return err
		}
	}

	return nil
}

// getGUID reads the GUID from a file.
func (s *SecureBoot) getGUID(path string) (uuid.UUID, error) {
	b, err := os.ReadFile(path)
	if err != nil {
		return [16]byte{}, err
	}
	u, err := uuid.ParseBytes(b)
	if err != nil {
		return [16]byte{}, err
	}
	return u, err
}

// checkImmutable checks if the EFI variables are immutable.
func checkImmutable() error {
	for _, file := range sbctl.EfivarFSFiles {
		err := sbctl.IsImmutable(file)
		if errors.Is(err, sbctl.ErrImmutable) {
			return err
		} else if errors.Is(err, sbctl.ErrNotImmutable) {
			continue
		} else if err != nil {
			// ignore
		}
	}
	return nil
}
