package bootloader

import (
	"fmt"
	"os"
	"path"
	"strconv"
	"strings"
)

// LoaderConfig implements load and write methods for loader.conf
type LoaderConfig struct {
	Timeout     int
	DefaultBoot string
	bootDir     string
}

// NewLoaderConfig returns a LoaderConfig with the current settings of the loader.conf
func NewLoaderConfig(bootDir string) (*LoaderConfig, error) {
	loaderConfig := &LoaderConfig{bootDir: bootDir}

	var data []byte
	data, err := os.ReadFile(loaderConfig.Path())
	if err != nil {
		return nil, err
	}

	lines := strings.Split(string(data), "\n")
	for _, line := range lines {
		if strings.HasPrefix(line, "#") {
			continue
		}

		parts := strings.SplitN(line, " ", 2)
		if len(parts) != 2 {
			continue
		}

		switch parts[0] {
		case "timeout":
			loaderConfig.Timeout, err = strconv.Atoi(parts[1])
			if err != nil {
				return nil, err
			}
		case "default":
			loaderConfig.DefaultBoot = parts[1]
		}
	}

	return loaderConfig, nil
}

// Path returns the path to the systemd-boot loader config.
func (l *LoaderConfig) Path() string {
	return path.Join(l.bootDir, "loader/loader.conf")
}

// Write updates the timeout and default boot in `loader.conf`.
func (l *LoaderConfig) Write() error {
	f, err := os.OpenFile(l.Path(), os.O_WRONLY|os.O_CREATE, 0644)
	if err != nil {
		return err
	}
	defer f.Close()

	if l.Timeout > 0 {
		_, err = fmt.Fprintf(f, "timeout %v\n", l.Timeout)
		if err != nil {
			return err
		}
	}

	_, err = fmt.Fprintf(f, "default %s\n", l.DefaultBoot)
	return err
}
