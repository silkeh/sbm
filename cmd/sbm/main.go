package main

import (
	"fmt"
	"log"
	"os"

	"github.com/urfave/cli/v2"

	"gitlab.com/silkeh/sbm/commands"
	"gitlab.com/silkeh/sbm/config"
)

var app = &cli.App{
	Version:                config.Version,
	Usage:                  "manage boot configuration",
	UseShortOptionHandling: true,
	Commands: []*cli.Command{
		{
			Name:   "report-booted",
			Usage:  "report that the currently running kernel has successfully booted",
			Action: commands.ReportBooted,
		},
		{
			Name:   "update",
			Usage:  "update the boot configuration",
			Action: commands.Update,
		},
		{
			Name:   "get-timeout",
			Usage:  "get the bootloader timeout",
			Action: commands.GetTimeout,
		},
		{
			Name:   "set-timeout",
			Usage:  "set the bootloader timeout",
			Action: commands.SetTimeout,
		},
		{
			Name:   "list-kernels",
			Usage:  "list the currently available kernels",
			Action: commands.ListKernels,
		},
		{
			Name:   "set-kernel",
			Usage:  "set the kernel for the next boot",
			Action: commands.SetKernel,
		},
		{
			Name:   "install-kernel",
			Usage:  "install a kernel to the bootloader",
			Action: commands.InstallKernel,
		},
		{
			Name:   "mount-boot",
			Usage:  "mount the boot partition",
			Action: commands.MountBoot,
		},
		{
			Name:   "enable-secure-boot",
			Usage:  "enable secure boot",
			Action: commands.EnableSecureBoot,
			Flags: []cli.Flag{
				&cli.BoolFlag{
					Name:        "with-ms",
					Aliases:     []string{"m"},
					Usage:       "also load microsoft keys",
					Destination: &commands.Flags.WithMicrosoft,
				},
			},
		},
	},
	Flags: []cli.Flag{
		&cli.StringFlag{
			Name:        "path",
			Aliases:     []string{"p"},
			Usage:       "base prefix for boot management",
			Value:       "/",
			Destination: &commands.Flags.Prefix,
		},
		&cli.BoolFlag{
			Name:        "image",
			Aliases:     []string{"i"},
			Usage:       "run in image mode",
			Destination: &commands.Flags.ImageMode,
		},
		&cli.BoolFlag{
			Name:        "no-efi-update",
			Aliases:     []string{"n"},
			Usage:       "do not update any EFI variables",
			Destination: &commands.Flags.NoEFI,
		},
		&cli.BoolFlag{
			Name:        "verbose",
			Aliases:     []string{"v"},
			Usage:       "verbose mode",
			Destination: &commands.Flags.Verbose,
		},
	},
}

func main() {
	// Disable logging prefixes
	log.SetFlags(0)

	// Make the version command `-V`
	cli.VersionFlag = &cli.BoolFlag{
		Name:    "version",
		Aliases: []string{"V"},
		Usage:   "print the version",
	}

	// Run the application
	err := app.Run(os.Args)
	if err != nil {
		_, _ = fmt.Fprintln(os.Stderr, "Error:", err)
		os.Exit(1)
	}
}
